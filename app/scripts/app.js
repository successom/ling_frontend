'use strict';

/**
 * @ngdoc overview
 * @name lingFrontendApp
 * @description
 * # lingFrontendApp
 *
 * Main module of the application.
 */
angular
  .module('lingFrontendApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ng-token-auth',
    'ui.bootstrap',
    'dialogs.main',
    'flash',
    'pascalprecht.translate',
    'LocalStorageModule',
    'ui.grid',
    'ui.grid.selection',
    'ngAudio',
    'slick',
    'underscore',
    'ngDragDrop',
    'ngTagsInput',
    'ngTagCloud'
  ])

  .constant("appConfig", {
      "backendUrl": "http://localhost:3000"
  })

  .config(function ($routeProvider, $authProvider, appConfig) {
    $routeProvider
      .when('/', {
        controller: 'MainCtrl',
        templateUrl: 'views/main.html',
        pageTitle: 'Ling',
        controllerAs: 'main'
      })
      .when('/vocabulary', {
        controller: 'VocabularyCtrl',
        templateUrl: 'views/vocabulary.html',
        pageTitle: 'Vocabulary',
        controllerAs: 'vocabulary'
      })
      .when('/about', {
        controller: 'AboutCtrl',
        templateUrl: 'views/about.html',
        pageTitle: 'About',
        controllerAs: 'about'
      })
      .when('/policies', {
        controller: 'Policies',
        templateUrl: 'views/policies.html',
        pageTitle: 'Policies',
        controllerAs: 'policies'
      })
      .otherwise({
        redirectTo: '/'
      });

    $authProvider.configure({
        apiUrl:             appConfig.backendUrl,
        validateOnPageLoad: true,
        authProviderPaths: {
          facebook: '/auth/facebook',
          google: '/auth/google_oauth2'
        }
    });
  })
  .config(function (localStorageServiceProvider) {
    localStorageServiceProvider
      .setPrefix('lingFrontendApp')
      .setStorageType('sessionStorage');
  });
