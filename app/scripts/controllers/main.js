'use strict';

angular.module('lingFrontendApp')

  .directive('modaldraggable', function () {
    return function (scope, element) {
      element= angular.element(document.getElementsByClassName("modal-dialog"));
      element.draggable();
    };
  })

  .directive('whenScrolled', function() {
      return function(scope, elm, attr) {
          var raw = elm[0];
          elm.bind('scroll', function() {
              if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                  scope.$apply(attr.whenScrolled);
              }
          });
      };
  })

  .controller('MainCtrl', function($rootScope, $scope, $uibModal, $translate, $auth, localStorageService){

    $rootScope.i18nTranslate = function(word){
      return $translate.instant(word);
    };

    $scope.launch = function(which){
      switch(which){
        case 'login':
          $scope.openLoginDialog();
          break;
        case 'sign_up':
          $scope.openSignUpDialog();
          break;
        case 'facebook_login':
          $scope.facebookLogin();
          break;
        case 'google_login':
          $scope.googleLogin();
          break;
      }
    };

    $scope.handleSignOutBtnClick = function() {
      $scope.signOut();
      $rootScope.currentUserDictionary = null;
      localStorageService.set('currentUserDictionary', $rootScope.currentUserDictionary);
      $scope.current_language = null;
    };

    $rootScope.$on('auth:logout-success', function() {
      $rootScope.success($translate.instant('thanks_for_using'));
    });

    $rootScope.$on('auth:logout-error', function(reason) {
      $rootScope.danger(reason.errors[0]);
    });

    $scope.openLoginDialog = function () {
      $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'loginDialog.html',
        controller: 'userLoginCtrl',
        size: 'sm',
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });
    };

    $scope.openSignUpDialog = function () {
      $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'signUpDialog.html',
        controller: 'userSignUpCtrl',
        size: 'sm',
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });
    };

    $scope.facebookLogin = function() {
      $auth.authenticate('facebook')
        .then(function() {
        })
        .catch(function() {
        });
    };

    $scope.googleLogin = function() {
      $auth.authenticate('google')
        .then(function() {
          // handle success
        })
        .catch(function() {
          // handle errors
        });
    };
  })

  .controller('userLoginCtrl',function($scope, $rootScope, $uibModalInstance, $translate){

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    // $scope.handleLoginBtnClick = function() {
    //   $auth.submitLogin($scope.loginForm);
    // };

    $rootScope.$on('auth:login-success', function(ev, user) {
      $uibModalInstance.close();
      $rootScope.success($translate.instant('user') + '  ' + user.email + '  ' + $translate.instant('login_success'));
    });

    $rootScope.$on('auth:login-error', function(ev, reason) {
      $rootScope.danger(reason.errors[0]);
    });
  })

  .controller('userSignUpCtrl',function($scope, $rootScope, $uibModalInstance, $translate, $auth){

    $scope.handleRegBtnClick = function() {
      $auth.submitRegistration($scope.registrationForm);
    };

    $rootScope.$on('auth:registration-email-success', function(ev, user) {
      $rootScope.success($translate.instant('user') + ' ' + user.email + '  ' +  $translate.instant('signed_up_success'));
      $uibModalInstance.close();
    });

    $rootScope.$on('auth:registration-email-error', function(ev, reason) {
      var error = '';
      _.each(_.uniq(reason.errors.full_messages), function(err){ error = error + err + '!\n'; });
      $rootScope.danger(error);
    });

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  })

  .controller('FlashCtrl', ['$rootScope', 'Flash', function ($rootScope, Flash) {
    $rootScope.success = function (message) {
      Flash.create('success', message);
    };
    $rootScope.info = function (message) {
      Flash.create('info', message);
    };
    $rootScope.warning = function (message) {
      Flash.create('warning', message);
    };
    $rootScope.danger = function (message) {
      Flash.create('danger', message);
    };
  }]);
