'use strict';

angular.module('lingFrontendApp')

  .factory('AssociationImage', function($resource, appConfig) {
    return $resource(appConfig.backendUrl + '/vocabulary/api/v1/association_images');
  })

  .factory('Tag', function($resource, appConfig) {
    return $resource(appConfig.backendUrl + '/vocabulary/api/v1/translation_tags');
  })

  .controller('wordCardCtrl', function($rootScope, $scope, $uibModalInstance, $uibModal, AssociationImage){

    $rootScope.cancelCard = function () {
      $uibModalInstance.dismiss('cancel');
    };

    $scope.getImages = function(card){
      return AssociationImage.query({word:           card.word_from,
                                     translation_id: card.translation_id,
                                     dict_id:        card.dict_id}).
        $promise.then(function (result) {
          $rootScope.imageAssociations = [];
          result.forEach(function(item) {
            $rootScope.imageAssociations.push(item.MediaUrl);
          });
        $scope.openImgDialog();
        });
    };

    $scope.switchEntity = function(cardId, destination){
      if (destination === 'next') {
        $rootScope.rowEntity = $rootScope.userDictionaryTranslations[$rootScope.wordIndex + 1];
      }
      else {
        $rootScope.rowEntity = $rootScope.userDictionaryTranslations[$rootScope.wordIndex - 1];
      }
      $rootScope.wordIndex = _.indexOf($rootScope.userDictionaryTranslations, $rootScope.rowEntity);
    };

    $rootScope.delCard = function() {
      if (confirm($rootScope.i18nTranslate('sure_delete_card'))) {
        $rootScope.delUserTranslation($rootScope.rowEntity.id);
        $rootScope.cancelCard();
      }
    };

    $scope.openImgDialog = function () {

      $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'imageAssociation.html',
        controller: 'imageAssociationCtrl',
      });
    };
  })

  .controller('imageAssociationCtrl', function($rootScope, $scope, $uibModalInstance, AssociationImage){

    $scope.cancelImgDialog = function () {
      $uibModalInstance.dismiss('cancel');
      $rootScope.imageAssociations = [];
    };

    $scope.saveImage = function(imgUrl){
      var associationImage = new AssociationImage({users_dictionary_id: $rootScope.rowEntity.users_dictionary_id,
                                                   translation_id:      $rootScope.rowEntity.translation_id,
                                                   word:                $rootScope.rowEntity.word_from,
                                                   dict_id:             $rootScope.rowEntity.dict_id,
                                                   image_url:           imgUrl});
      return associationImage.$save(function (result) {
        $rootScope.rowEntity.association_img_url = result.new_image_url;
        $scope.cancelImgDialog();
      });
    };
  })

  .controller('tagCtrl', function($rootScope, $scope, Tag){
    $scope.loadTags = function($query, card) {
      return Tag.query({template: $query, dict_id: card.dict_id}).
        $promise.then(function (result) {
          return result;
        });
    };

    $scope.newTagSubmit= function (tag, card) {
      var newTag = new Tag({tag:                 tag.text,
                            dict_id:             card.dict_id,
                            translation_id:      card.translation_id,
                            users_dictionary_id: card.users_dictionary_id});
      return newTag.$save(function (result) {
        $scope.card.tags.pop();
        $scope.card.tags.push(result);
        $rootScope.fetchUserDictionaryTranslations();
      });
    };

    $scope.delTag= function (tag, card) {
      if (tag.owner === true) {
        var tagForDelete = new Tag();
        tagForDelete.$delete({tag_id:         tag.id,
                              dict_id:        card.dict_id,
                              translation_id: card.translation_id
                            }, function(result) {
                              $rootScope.fetchUserDictionaryTranslations();
                            });
      }
      else {
        alert("You can't delete this tag. You are not an owner");
        return false;
      }
    };
  });
