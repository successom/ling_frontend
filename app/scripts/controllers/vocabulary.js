'use strict';

angular.module('lingFrontendApp')
  .factory('UserDictionary', function($resource, appConfig) {
    return $resource(appConfig.backendUrl + '/vocabulary/api/v1/user_dictionaries');
  })

  .factory('UserDictionaryTranslation', function($resource, appConfig) {
    return $resource(appConfig.backendUrl + '/vocabulary/api/v1/user_dictionary_translations');
  })

  .factory('Translation', function($resource, appConfig) {
    return $resource(appConfig.backendUrl + '/vocabulary/api/v1/translations');
  })

  .controller('newWordCtrl', [ '$rootScope', '$scope', function($rootScope, $scope){

    $('#new-word').hide();

    $scope.openNewWordForm = function() {
      $('#own-translation').val('');
      $('#new-word').show();
    };

    $scope.newWordSubmit = function() {
      var item = {"external_translation": "true", "word_to": $scope.newWord, "word_from": $rootScope.wordMatcher,"translation_id": 0};
      $scope.addUserTranslation(item);
      $('#new-word').hide();
    };

    $rootScope.resetScrollCounter = function() {
      $scope.scrollCounter = 20;
    };

    $scope.loadMore = function() {
      $scope.scrollCounter = $scope.scrollCounter + 20;
    };

    $scope.loadMatches = function() {
      return $scope.matches.slice(0, $scope.scrollCounter);
    };

    $rootScope.resetScrollCounter();
  }])

  .controller('VocabularyCtrl', [ '$rootScope', '$scope', 'UserDictionary', 'UserDictionaryTranslation', 'Translation', '$http', 'localStorageService', 'dialogs', 'ngAudio', '$uibModal', 'appConfig',function($rootScope, $scope, UserDictionary, UserDictionaryTranslation, Translation, $http, localStorageService, dialogs, ngAudio, $uibModal, appConfig) {

    $scope.fetchUserDictionaries = function(top) {
      return UserDictionary.query({user_id: $rootScope.user.id}).
        $promise.then(function (result) {
          $rootScope.userDictionaries = result;
          $rootScope.countUserDictionaries = $rootScope.userDictionaries.length;
          localStorageService.set('countUserDictionaries', $rootScope.countUserDictionaries);
          if (top === 'true') {
            $rootScope.currentUserDictionary = $rootScope.userDictionaries[0];
            localStorageService.set('currentUserDictionary', $rootScope.currentUserDictionary);
            $rootScope.fetchUserDictionaryTranslations();
          }
        });
    };

    $rootScope.currentUserDictionary = localStorageService.get('currentUserDictionary');
    $rootScope.countUserDictionaries = localStorageService.get('countUserDictionaries');

    if ($rootScope.currentUserDictionary === null) {
      $scope.fetchUserDictionaries('true');
    }

    $scope.fetchUnusedDictionaries = function() {
      var response = $http.get(appConfig.backendUrl + '/vocabulary/api/v1/dictionaries/' + $rootScope.user.id);
      response.success(function(data) {
        $scope.unusedDictionaries = data;
      });
      response.error(function(data) {
        alert("Error." + data);
      });
    };

    $scope.addUserDictionary = function(dictId){
      if (dictId > 0) {
        var userDictionary = new UserDictionary({user_id: $rootScope.user.id, dictionary_id: dictId});
        return userDictionary.$save(function () {
          $scope.fetchUserDictionaries('true');
          });
      }
    };

    $scope.delUserDictionary = function(userDictId){
      if (confirm("Are you sure to delete dictionary?. You will lost all translations!")) {
        var userDictionary = new UserDictionary();
        return userDictionary.$delete({id: userDictId}, function () {
          $scope.fetchUserDictionaries('true');
          });
      }
    };

    $scope.selectUserDictionary = function(dictionary){
      $rootScope.currentUserDictionary = JSON.parse(dictionary);
      localStorageService.set('currentUserDictionary', $rootScope.currentUserDictionary);
      $rootScope.fetchUserDictionaryTranslations();
    };

    $rootScope.fetchUserDictionaryTranslations = function() {
      return UserDictionaryTranslation.query({user_dictionary_id: $rootScope.currentUserDictionary.id,
                                              dict_id:            $rootScope.currentUserDictionary.dict_id,
                                              from:               $rootScope.currentUserDictionary.from,
                                              to:                 $rootScope.currentUserDictionary.to
                                            }).
        $promise.then(function (result) {
          $rootScope.userDictionaryTranslations = result[0].translations;
          $rootScope.gridOptions.data = $rootScope.userDictionaryTranslations;
          $rootScope.tagCloud = result[1].tags;
        });
    };

    if ($rootScope.currentUserDictionary !== null) {
      $rootScope.tagCloud = [];
      $rootScope.fetchUserDictionaryTranslations();
    }

    $scope.fetchTranslations = function(word) {
      $('#new-word').hide();
      $rootScope.resetScrollCounter();
      return Translation.query({word:    word,
                                dict_id: $rootScope.currentUserDictionary.dict_id,
                                from:    $rootScope.currentUserDictionary.from,
                                to:      $rootScope.currentUserDictionary.to,
                                             }).
        $promise.then(function (result) {
          result.unshift({'id':'0','word_from':word,'word_to':$rootScope.i18nTranslate('add_new_translation')});
          return result;
        });
    };

    $scope.addUserTranslation = function($item) {
      var UserDictionaryTransl = new UserDictionaryTranslation({translation_id:       $item.translation_id,
                                                                users_dictionary_id:  $rootScope.currentUserDictionary.id,
                                                                dict_id:              $rootScope.currentUserDictionary.dict_id,
                                                                word_from:            $item.word_from,
                                                                word_to:              $item.word_to,
                                                                from:                 $rootScope.currentUserDictionary.from,
                                                                to:                   $rootScope.currentUserDictionary.to,
                                                                external_translation: $item.external_translation
                                                              });
      UserDictionaryTransl.$save(function() {
                                  $rootScope.fetchUserDictionaryTranslations();
                                 }
        );
    };

    $rootScope.delUserTranslation = function(userTranslationId) {
      var UserDictionaryTransl = new UserDictionaryTranslation();
      UserDictionaryTransl.$delete({id:      userTranslationId,
                                    dict_id: $rootScope.currentUserDictionary.dict_id
                                  }, function() {
                                    $rootScope.fetchUserDictionaryTranslations();
                                  });
    };

    $rootScope.gridOptions = {
      enableGridMenu: true,
      enableRowSelection: true,
      onRegisterApi: function(gridApi){
        $rootScope.gridApi = gridApi;
        $rootScope.gridApi.grid.registerRowsProcessor( $rootScope.singleFilter, 200 );
      },
      columnDefs: [
          { name: ' ',
            cellTemplate:'<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a href class=" glyphicon glyphicon-volume-up" ng-click="grid.appScope.getSounds(row.entity.word_from);"></a></div>',
            width: '30'

          },
          { name: $rootScope.i18nTranslate('word'),
            cellTemplate:'<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a href ng-click="grid.appScope.rowDialogOpen(row)">{{row.entity.word_from}}</a></div>'
          },
          { name: $rootScope.i18nTranslate('translations'), field: 'word_to' }
        ],
      data: $rootScope.userDictionaryTranslations,
      gridMenuCustomItems: [
        {
          title: 'Delete selected',
          action: function ($event) {
            $scope.deleteSelected();
          }
        },
        {
          title: 'Clear selections',
          action: function ($event) {
            $scope.gridApi.selection.clearSelectedRows();
          }
        }
      ]
    };

    $rootScope.wordFilter = function(matcher) {
      $rootScope.wordMatcher = matcher;
      $rootScope.gridApi.grid.refresh();
    };

    $rootScope.grigRefresh = function() {
      $rootScope.wordMatcher = '';
      $rootScope.gridApi.grid.refresh();
      $('#new-word').hide();
    };

    $rootScope.singleFilter = function( renderableRows ){
      var matcher = new RegExp($rootScope.wordMatcher);
      renderableRows.forEach( function( row ) {
        var match = false;
        [ 'word_from' ].forEach(function( field ){
          if ( row.entity[field].match(matcher) ){
            match = true;
          }
        });
        if ( !match ){
          row.visible = false;
        }
      });
      return renderableRows;
    };

    $scope.deleteSelected = function(){
     var rowsForDelete = $scope.gridApi.selection.getSelectedRows();
     if ((rowsForDelete.length > 0) && (confirm('Are you sure to delete ' + rowsForDelete.length + ' words?'))) {
        angular.forEach(rowsForDelete, function (data, index) {
          $scope.delUserTranslation(data.id);
          $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
        });
      }
    };

    $scope.rowDialogOpen = function (row) {
      $rootScope.rowEntity = row.entity;
      $rootScope.wordIndex = _.indexOf($rootScope.userDictionaryTranslations, $rootScope.rowEntity);
      $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'wordCard.html',
        controller: 'wordCardCtrl'

      });
    };

    $rootScope.getSounds = function(word){
      var words = word.split(" ");
      $scope.sounds = _.map(words, function(word){ return ngAudio.load('https://ssl.gstatic.com/dictionary/static/sounds/de/0/'+ word +'.mp3') });
      $scope.playSounds($scope.sounds[0]);
    };

    $scope.playSounds = function(sound){
      $scope.sound = sound;
      $scope.sound.play();
      $scope.sound.complete(function() {
                                         $scope.sound.stop();
                                         $scope.sounds.shift();
                                         if ($scope.sounds.length > 0) {
                                           $scope.playSounds($scope.sounds[0]);
                                         }
                                       });
    };
  }]);
