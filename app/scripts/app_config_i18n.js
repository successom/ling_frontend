'use strict';

angular.module('lingFrontendApp')
  .config(['$translateProvider', function ($translateProvider) {
    $translateProvider.translations('en', {
      'Ling': 'Ling',
      'TITLE': 'Hello',
      'FOO': 'Always a pleasure scaffolding your apps.',
      'user': 'User',
      'cancel': 'Cancel',
      'log_in': 'Login',
      'sign_in': 'Email login',
      'sign_in_facebook': 'Facebook login',
      'sign_in_google': 'Google login',
      'logout': 'Logout',
      'register': 'Register',
      'sign_up': 'Sign up',
      'email': 'email',
      'password': 'password',
      'password_confirmation': 'password confirmation',
      'home': 'Home',
      'about': 'About',
      'select_language': 'Select language',
      'thanks_for_using': 'Thanks for using! Come back!',
      'login_success': 'logged in successfully!',
      'signed_up_success': 'signed up successfully!',
      'my_vocabularies': 'My dictionaries',
      'add_new_dictionary': 'Add new dictionary',
      'del_dictionary': 'Remove dictionary',
      'select_dictionary': 'Select dictionary',
      'сurrent_dictionary': 'Current dictionary:',
      'enter_new_translation': 'Enter here new translation',
      'type_the_word': 'Type the word here',
      'add': 'Add',
      'no_results_found': 'No results found',
      'add_new_translation': 'Add own translation',
      'word': 'word',
      'translations': 'translation',
      'original_word': 'Original word',
      'get_sound': 'Get sound of word',
      'word_transcription': 'Word transcription',
      'word_translation': 'Word translation',
      'another_image': 'Click for select another image association',
      'remove_card': 'Remove word card',
      'sure_delete_card': 'Are you sure to delete this word card?',
      'word_tags': 'tags'
    });

    $translateProvider.translations('de', {
      'Ling': 'Ling',
      'TITLE': 'Hallo',
      'FOO': 'Immer ein Vergnügen Gerüst Ihrer Apps.',
      'user': 'Benutzer',
      'cancel': 'Absagen',
      'log_in': 'Anmelden',
      'sign_in': 'Email anmelden',
      'sign_in_facebook': 'Facebook anmelden',
      'sign_in_google': 'Google anmelden',
      'logout': 'Abmelden',
      'register': 'Registrieren',
      'sign_up': 'Anmeldung',
      'email': 'email',
      'password': 'passwort',
      'password_confirmation': 'passwort bestätigung',
      'home': 'Home',
      'about': 'Sie',
      'select_language': 'Sprache auswählen',
      'thanks_for_using': 'Danke fuer die Benutzung! Komm zurück!',
      'login_success': 'anmelden erfolgreich!',
      'signed_up_success': 'unterzeichnet erfolgreich!',
      'my_vocabularies': 'Mein Wörterbücher',
      'add_new_dictionary': 'In neuen Wörterbuch',
      'del_dictionary': 'Entfernen Wörterbuch',
      'select_dictionary': 'Select Wörterbuch',
      'сurrent_dictionary': 'aktuelle Wörterbuch:',
      'enter_new_translation': 'Geben Sie neue Übersetzung',
      'type_the_word': 'Geben Sie das Wort hier',
      'add': 'Hinzufügen',
      'no_results_found': 'keine Ergebnisse gefunden',
      'add_new_translation': 'In Eigene Übersetzung',
      'word': 'Wort',
      'translations': 'Übersetzung',
      'original_word': 'Ursprüngliche Wort',
      'get_sound': 'Holen Klang des Wortes',
      'word_transcription': 'Word-Transkription',
      'word_translation': 'Wort-Übersetzung',
      'another_image': 'Klicken Sie hier für Sie ein anderes Bild Verband',
      'remove_card': 'Entfernen Sie Wortkarte',
      'sure_delete_card': 'Sind Sie sicher, dieses Wort Karte löschen?',
      'word_tags': 'etikett'
    });

    $translateProvider.translations('ru', {
      'Ling': 'Ling',
      'TITLE': 'Привет',
      'FOO': 'Всегда приятно генерировать ваши приложения.',
      'user': 'Пользователь',
      'cancel': 'Отменить',
      'log_in': 'Войти',
      'sign_in': 'Email вход',
      'sign_in_facebook': 'Facebook вход',
      'sign_in_google': 'Google вход',
      'logout': 'Выйти',
      'register': 'Зарегистрироваться',
      'sign_up': 'Зарегистрироваться',
      'email': 'почтовый адрес',
      'password': 'пароль',
      'password_confirmation': 'подтверждение пароля',
      'home': 'Домашняя',
      'about': 'О проекте',
      'select_language': 'Выберите язык',
      'thanks_for_using': 'Спасибо за то, что ты с нами! Возвращайся!',
      'login_success': 'вошел успешно!',
      'signed_up_success': 'зарегистрировался успешно!',
      'my_vocabularies': 'Мои словари',
      'add_new_dictionary': 'Добавить новый словарь',
      'del_dictionary': 'Удалить словарь',
      'select_dictionary': 'Выберите словарь',
      'сurrent_dictionary': 'Выбран словарь:',
      'enter_new_translation': 'Введите свой перевод',
      'type_the_word': 'Введите слово',
      'add': 'Добавить',
      'no_results_found': 'Не найдено результатов',
      'add_new_translation': 'Добавить перевод',
      'word': 'Слово',
      'translations': 'Перевод',
      'original_word': 'Изучаемое слово',
      'get_sound': 'Прослушать',
      'word_transcription': 'Транскрипция',
      'word_translation': 'Перевод',
      'another_image': 'Щелкните для выбора другой картинки',
      'remove_card': 'Удалить словарную карточку',
      'sure_delete_card': 'Вы действительно хотите удалить словарную карточку?',
      'word_tags': 'теги'
    });

    $translateProvider.preferredLanguage('en');
    $translateProvider.useCookieStorage();
    $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
  }])

  .controller('localeCtrl', ['$translate', '$scope', function ($translate, $scope) {
    $scope.languages = [
      'ru',
      'en',
      'de'
    ];

    $scope.changeLanguage = function (key) {
      $translate.use(key);
    };

    $scope.current_language = function(){
      $translate.use();
    };
  }]);
